package kpi.lab3;

import dnl.utils.text.table.TextTable;

import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

class StorageView {
    private String[] columns = new String[]{
        "Registration number",
        "Name",
        "Model",
        "Quantity",
        "Manufacturer",
        "Price",
        "Manufactured"
    };

    Product[] fillStorage() {
        return  new Product[]{
                new Product("CLE.PRE.91517352", "High Pressure Cleaner", "QL-1800C", 4, "POWERWASH",66.71, new Date()),
                new Product("MAT.ELE.61518786", "Electric Wire Rope Hoist", "PA-500", 6, "Fulcrum", 72.35, new Date()),
                new Product("MAT.PAL.91518814", "Hand Pallet Truck", "HPT2T", 10, "Fulcrum", 123.54, new Date()),
                new Product("POW.DRI.91521152", "Electric Drill", "PHED10", 8, "PowerHouse", 16.28, new Date()),
                new Product("POW.MAR.21521143", "Marble Cutter", "PHMC110", 3, "PowerHouse", 31.16, new Date()),
                new Product("MAT.ELE.21518790", "Electric Wire Rope Hoist", "PA-1200", 4, "Fulcrum", 109.65, new Date()),
                new Product("SA.FA.EL.492211", "Electric Wire Rope Hoist", "PA-800", 6, "Cranlik", 90.35, new Date()),
                new Product("CLE.PRE.101517367", "High Pressure Cleaner", "PW-VAN", 3, "POWERWASH", 57.84, new Date()),
                new Product("CLE.PRE.31612080", "High Pressure Cleaner", "Universal Aquatak 135", 5, "Bosch", 154.73, new Date()),
                new Product("CL.HO.1434665", "High Pressure Cleaner", "Washer K2 Compact", 8, "Karcher", 65.58, new Date()),
        };
    }

    void printStorage(Product[] products) {
        System.out.println("Storage:");
        Object data[][] = new Object[products.length][columns.length];
        for (int i = 0; i < products.length; i++) {
                data[i] = products[i].toStringsArray();
        }

        TextTable tt = new TextTable(columns, data);
        tt.setAddRowNumbering(true);
        tt.setSort(1);
        tt.printTable();
    }

    String readKey(String description) {
        Scanner in = new Scanner(System.in);
        System.out.print(description);
        return in.nextLine();
    }

    int readChoice() {
        System.out.println("Menu: " +
                "\n\t0 - print storage table" +
                "\n\t1 - search by manufacturer" +
                "\n\t2 - search by product name" +
                "\n\t3 - exit");

        int n;
        Scanner in = new Scanner(System.in);
        while (true)
            try {
                n = in.nextInt();
                break;
            }
            catch (InputMismatchException e) {
                in.nextLine();
                System.out.print("Please enter a valid integer: ");
            }
        return n;
    }
}
