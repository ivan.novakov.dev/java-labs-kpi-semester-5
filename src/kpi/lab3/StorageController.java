package kpi.lab3;

class StorageController {
    private Storage model;
    private StorageView view;

    StorageController() {
        this.view = new StorageView();
        this.model = new Storage(this.view.fillStorage());
    }

    void loop() {
        while (true) {
            int choice = view.readChoice();
            switch (choice) {
                case 0:
                    updateView();
                    break;
                case 1:
                    getManufacturersByProduct();
                    break;
                case 2:
                    getProductsByManufacturer();
                    break;
                case 3:
                    return;
            }
        }
    }

    private void updateView() {
        view.printStorage(model.products);
    }

    private void getProductsByManufacturer() {
        String manufacturer = view.readKey("Enter manufacturer to view his products: ");
        Product result[] = model.getProductsByManufacturer(manufacturer);
        if (result.length == 0) {
            System.out.println("There is no products of this manufacturer.");
            return;
        }
        view.printStorage(result);
    }

    private void getManufacturersByProduct() {
        String productName = view.readKey("Enter product name to view all providers: ");
        Product result[] = model.getManufacturersByProduct(productName);
        if (result.length == 0) {
            System.out.println("There is no product with this name.");
            return;
        }
        view.printStorage(result);
    }
}
