package kpi.lab3;

import org.jetbrains.annotations.NotNull;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;

public class Storage {
    Product[] products;
    private int size = 10;
    private int capacity = 0;
    private int step = 10;

    Storage(@NotNull Product[] products) {
        if (products.length > size) {
            size = products.length + 10;
        }
        this.products = new Product[size];
        capacity = products.length;
        System.arraycopy(products, 0, this.products, 0, products.length);
    }

    public void add(@NotNull Product product) {
        capacity += 1;
        if (capacity > size) {
            size += 10;
            Product[] old = products;
            products = new Product[size];
            System.arraycopy(old, 0, products, 0, old.length);
        }
        products[capacity - 1] = product;
    }

    Product[] getProductsByManufacturer(String manufacturer) {
        Stream<Product> result = Arrays.stream(products)
                .filter(element -> Objects.equals(element.manufacturer, manufacturer));

        return result.toArray(Product[]::new);

    }

    Product[] getManufacturersByProduct(String productName) {
        Stream<Product> result = Arrays.stream(products)
                .filter(element -> Objects.equals(element.name, productName));

        return result.toArray(Product[]::new);
    }

    @Override
    public String toString() {
        return "Storage{" +
                "products=" + (products == null ? null : Arrays.asList(products)) +
                '}';
    }
}
