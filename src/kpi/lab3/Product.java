package kpi.lab3;

import java.util.Date;

class Product {
    private String registrationNumber;
    String name;
    private String model;
    private int quantity;
    String manufacturer;
    private double price;
    private Date manufactured;

    Product(String registrationNumber, String name, String model, int quantity, String manufacturer, double price, Date manufactured) {
        this.registrationNumber = registrationNumber;
        this.name = name;
        this.model = model;
        this.quantity = quantity;
        this.manufacturer = manufacturer;
        this.price = price;
        this.manufactured = manufactured;
    }

    @Override
    public String toString() {
        return "Product{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", quantity=" + quantity +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", manufactured=" + manufactured +
                '}';
    }

    String[] toStringsArray() {
        return new String[]{
                registrationNumber,
                name,
                model,
                Integer.toString(quantity),
                manufacturer,
                Double.toString(price),
                manufactured.toString()
        };
    }
}
