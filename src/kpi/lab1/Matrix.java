package kpi.lab1;

import java.text.DecimalFormat;

public class Matrix {

    private short[][] matrix;

    Matrix(short n) {
        this.matrix = new short[n][n];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (short) (Math.random()*10);
            }
        }
    }
    short findCol() {
        short max = Short.MIN_VALUE;
        short column = 0;

        for (short[] aMatrix : matrix) {
            for (short j = 0; j < aMatrix.length; j++) {
                if (max < aMatrix[j]) {
                    max = aMatrix[j];
                    column = j;
                }
            }
        }

        return column;
    }
    void foo() {
        short index = this.findCol();

        for (int i = 0; i < matrix.length; i++) {
            short[] old = matrix[i];
            matrix[i] = new short[matrix[i].length - 1];
            for (int j = 0; j < old.length - 1; j++) {
                if (j < index)
                    matrix[i][j] = old[j];
                else
                    matrix[i][j] = old[j + 1];
            }
        }
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        DecimalFormat decimalFormat = new DecimalFormat("####");
        for (short[] row : matrix) {
            for (short value : row)
                stringBuilder.append(decimalFormat.format(value)).append(" ");
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }
}
