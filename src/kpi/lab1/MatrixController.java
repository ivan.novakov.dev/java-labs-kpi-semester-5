package kpi.lab1;

class MatrixController {

    private Matrix model;
    private MatrixView view;

    MatrixController() {
        MatrixView view = new MatrixView();

        this.model = new Matrix(view.readSize());
        this.view = view;
    }

    void foo() {
        model.foo();
    }

    void updateView() {

        view.printMatrix(model.toString());
    }
}
