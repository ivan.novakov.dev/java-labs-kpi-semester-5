package kpi.lab1;

import java.util.InputMismatchException;
import java.util.Scanner;

class MatrixView {
    void printMatrix(String matrix) {
        System.out.println("Matrix:");
        System.out.println(matrix);
    }

    short readSize() {
        System.out.print("Enter size of matrix: ");

        short n;
        Scanner in = new Scanner(System.in);
        while (true)
            try {
                n = in.nextShort();
                break;
            }
            catch (InputMismatchException e) {
                in.nextLine();
                System.out.print("Please enter a valid short: ");
            }
        return n;
    }
}
