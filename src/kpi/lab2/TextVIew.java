package kpi.lab2;

import java.util.InputMismatchException;
import java.util.Scanner;

class TextView {

    void printText(String text) {

        System.out.println("Text:");
        System.out.println(text);
    }

    String readText()
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter text: ");
        return in.nextLine();
    }

    char readChar() {
        System.out.print("Enter character to replace: ");
        Scanner in = new Scanner(System.in);
        return in.next().charAt(0);


    }

    int readInt() {
        int n;
        Scanner in = new Scanner(System.in);

        System.out.print("Enter which position will be replaced: ");
        while (true)
        try {
            n = in.nextInt();
            break;
        }
        catch (InputMismatchException e) {
            in.nextLine();
            System.out.print("Please enter a valid int: ");
        }
        return n;
    }
}
