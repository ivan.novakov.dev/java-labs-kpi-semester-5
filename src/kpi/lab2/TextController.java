package kpi.lab2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TextController {
    private Text model;
    private TextView view;

    TextController() {
        TextView view = new TextView();

        this.model = new Text(view.readText());
        this.view = view;
    }

    void foo() {
        int n = view.readInt();
        char c = view.readChar();
        model.foo(n, c);
    }

    void updateView() {

        view.printText(model.toString());
    }
}
