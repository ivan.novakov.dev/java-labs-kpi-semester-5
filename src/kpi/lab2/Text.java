package kpi.lab2;

public class Text {
    private String text;

    void foo(int n,char c) {

        String split[] = this.text.split("\\b|(?<=[^\\w\\s])");
        for (int i = 0; i < split.length; i++) {
            if (Character.isLetter(split[i].charAt(0)) && split[i].length() >= n) {
                StringBuilder stringBuilder = new StringBuilder(split[i]);
                stringBuilder.setCharAt(n - 1, c);
                split[i] = stringBuilder.toString();
            }
        }

        StringBuilder result = new StringBuilder();
        for (String aSplit : split) {
            result.append(aSplit);
        }
        this.text = result.toString();
    }

    @Override
    public String toString() {
        return text;
    }

    public Text(String text) {
        this.text = text;
    }
}
